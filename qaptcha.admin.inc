<?php
/**
 * @file
 * Provides the qaptcha administration settings.
 */

/**
 * Form callback; administrative settings for qaptcha.
 */
function qaptcha_admin_settings_form() {

  $form = array();

  $form['qaptcha_auto_revert'] = array(
    '#type' => 'radios',
    '#title' => t('Auto Revert Qaptcha'),
    '#description' => t('Slider returns to the init-position, when the user has not dragged it to end.'),
    '#options' => array(TRUE => t('On'), FALSE => t('Off')),
    '#default_value' => variable_get('qaptcha_auto_revert', TRUE),
  );
  $form['qaptcha_txt_lock'] = array(
    '#type' => 'textfield',
    '#title' => t('Lock text'),
    '#default_value' => variable_get('qaptcha_txt_lock', t("Locked : form can't be submited")),
  );
  $form['qaptcha_txt_unlock'] = array(
    '#type' => 'textfield',
    '#title' => t('Unlock text'),
    '#default_value' => variable_get('qaptcha_txt_unlock', t('Unlocked : form can be submited')),
  );

  return system_settings_form($form);
}
