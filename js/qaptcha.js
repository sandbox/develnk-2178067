(function ($) {
    Drupal.behaviors.qaptcha = {
        attach: function (context) {
            $('.QapTcha').QapTcha({
                txtLock : Drupal.settings.qaptcha_txt_lock,
                txtUnlock : Drupal.settings.qaptcha_txt_unlock,
                autoRevert : parseInt(Drupal.settings.qaptcha_auto_revert),
                PHPfile : '/qaptcha'
            });
        }
    };
}(jQuery));
