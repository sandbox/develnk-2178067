QapTcha for Drupal
====================

The QapTcha is jquery plugin integrating in CAPTCHA system drupal.
For more information on what QapTcha is, please visit:
     http://www.myjqueryplugins.com/jquery-plugin/qaptcha

DEPENDENCIES
------------

* CAPTCHA module.
  https://drupal.org/project/captcha
* Some people have found that they also need to use jQuery Update module.
  https://drupal.org/project/jquery_update
* libraries module
  https://drupal.org/project/libraries


INSTALLITION
-------------
1. Download and unpack the Libraries module directory in your modules folder.
2. Download and unpack jquery_update module.
3. Download and unpack captcha module
4. Download and unpack the qaptcha plugin in "sites/all/libraries".
   https://github.com/develnk/qaptcha-fork.git
   Make sure the path to the plugin file becomes:
   "sites/all/libraries/qaptcha/jquery/QapTcha.jquery.min.js"
5. Download and unpack the qaptcha module directory in your modules folder
   (this will usually be "sites/all/modules/").
6. Configure jquery_update module (admin/config/development/jquery_update).
   Enable jquery version not below 1.7
7. Go to "Administer" -> "Modules" and enable the qaptcha module.


CONFIGURATION:
-------------
Go to "Configuration" -> "People" -> "CAPTCHA" to find
all the configuration options.
